import random
import matplotlib.pyplot as plt
from timeit import default_timer as timer

def multiplication(mas, const):
	mass = []
	for i in range(len(mas)):
		mass.append(mas[i] * const)
	return mass

def addition(mas1, mas2):
	mass = []
	for i in range(len(mas1)):
		mass.append(mas1[i] + mas2[i])
	return mass

def random_mas_int(var, bord1, bord2):
	mass = []
	for _ in range(var):
		mass.append(random.randint(bord1, bord2))
	return mass

def random_mas_float(var, bord1, bord2):
	mass = []
	for _ in range(var):
		mass.append(random.uniform(bord1, bord2))
	return mass

const = random.randint(50, 200)

mas_value, mas_time_1, mas_time_2 = [], [], []

for i in range(200000, 1000001, 100000):
	mas_1 = random_mas_int(i, 0, 100)
	mas_2 = random_mas_int(i, 0, 100)
	mas_3 = random_mas_float(i, 0, 100)
	mas_4 = random_mas_float(i, 0, 100)

	avg_val_1, avg_val_2 = 0, 0

	for _ in range(3):
		start_time = timer()
		n1 = addition(multiplication(mas_1, const), mas_2)
		avg_val_1 += timer() - start_time

		start_time = timer()
		n2 = addition(multiplication(mas_3, const), mas_4)
		avg_val_2 += timer() - start_time

	mas_value.append(i)
	mas_time_1.append(avg_val_1 / 3)
	mas_time_2.append(avg_val_2 / 3)

fig, ax = plt.subplots()
plt.plot(mas_value, mas_time_1, marker='o', label = 'int')
plt.plot(mas_value, mas_time_2, marker='o', label = 'float')
plt.title('График зависимости времени подсчета от длины вектора')
plt.legend(fontsize = 8, ncol = 2, facecolor = 'white', edgecolor = 'black', title = 'Прямые', title_fontsize = '8')
ax.set_xlabel('Длина вектора')
ax.set_ylabel('Время (с)')
plt.grid()
plt.show()
