summ = 42
while True:
	try:
		number = input()
		summ += 1 / int(number)
	except ValueError:
		print('Файл содержит данные не правильного формата! Проверьте, чтобы все числа были целые.')
		exit()
	except EOFError:
		break
	except ZeroDivisionError:
		print('Одно из чисел равно нулю!')
		exit()
print('Сумма ряда: ', summ)
