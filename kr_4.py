import random
import matplotlib.pyplot as plt
from timeit import default_timer as timer

def multiplication(matrix1, matrix2):
	for i in range(len(matrix1[0])):
		for j in range(len(matrix1[0])):
			matrix1[i][j] *= matrix2[i][j]
	return matrix1

def random_matrix_int(var, bord1, bord2):
	mass = [[0 for _ in range(var)] for _ in range(var)]
	for i in range(var):
		for j in range(var):
			mass[i][j] = random.randint(bord1, bord2)
	return mass

def random_matrix_float(var, bord1, bord2):
	mass = [[0 for _ in range(var)] for _ in range(var)]
	for i in range(var):
		for j in range(var):
			mass[i][j] = random.uniform(bord1, bord2)
	return mass

mas_value, mas_time_1, mas_time_2 = [], [], []

for i in range(1000, 3001, 250):
	matrix_1 = random_matrix_int(i, 0, 100)
	matrix_2 = random_matrix_int(i, 0, 100)
	matrix_3 = random_matrix_float(i, 0, 100)
	matrix_4 = random_matrix_float(i, 0, 100)

	avg_val_1, avg_val_2 = 0, 0

	for k in range(3):
		start_time = timer()
		n1 = multiplication(matrix_1, matrix_2)
		avg_val_1 += timer() - start_time

		start_time = timer()
		n2 = multiplication(matrix_3, matrix_4)
		avg_val_2 += timer() - start_time

	mas_value.append(i)
	mas_time_1.append(avg_val_1 / 3)
	mas_time_2.append(avg_val_2 / 3)

fig, ax = plt.subplots()
plt.plot(mas_value, mas_time_1, marker='o', label = 'int')
plt.plot(mas_value, mas_time_2, marker='o', label = 'float')
plt.title('График зависимости времени подсчета от размерности матрицы')
plt.legend(fontsize = 8, ncol = 2, facecolor = 'white', edgecolor = 'black', title = 'Прямые', title_fontsize = '8')
ax.set_xlabel('Размерность матрицы')
ax.set_ylabel('Время (с)')
plt.grid()
plt.show()
