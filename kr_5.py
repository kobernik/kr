import random
import sys
import matplotlib.pyplot as plt
from timeit import default_timer as timer

#Рандомное заполнение матрицы
def random_matrix_int(var, bord1, bord2):
	mass = [[0 for _ in range(var)] for _ in range(var)]
	for i in range(var):
		for j in range(var):
			mass[i][j] = random.randint(bord1, bord2)
	return mass

#Рандомное заполнение массива
def random_mas_int(var, bord1, bord2):
	mass = []
	for _ in range(var):
		mass.append(random.randint(bord1, bord2))
	return mass

#Нахождение определителя матрицы
def determinant_matrix(a):
	det = 0
	if len(a) == 1:
		det = a[0][0]
	if len(a) == 2:
		det = a[0][0] * a[1][1] - a[1][0] * a[0][1]
	else:
		k = 1
		for i in range(len(a)):
			det += k * a[0][i] * float(determinant_matrix(matrix_without_column_row(a, i)))
			k = -k
	return det

#Вырезание столбца и ряда относительно какого-то элемента (для дальнейшего нахождения определителя)
def matrix_without_column_row(a, num):
	mass = [[0 for _ in range(len(a) - 1)] for _ in range(len(a) - 1)]
	for i in range(len(mass)):
		for j in range(len(mass)):
			if j < num:
				mass[i][j] = a[i + 1][j]
			else:
				mass[i][j] = a[i + 1][j + 1]
	return mass

#Копирование матрицы (для дальнейшего изменения новой матрицы без изменения старой)
def copy_matrix(a):
	copy = [[0 for _ in range(len(a))] for _ in range(len(a))]
	for m in range(len(a)):
		for n in range(len(a)):
			copy[m][n] = a[m][n]	
	return copy

#Создание расширенной матрицы из matrix_1 и mas_2
def extended_matrix(a, b):
	for i in range(len(a)):
		a[i].append(b[i])
	return a

#Вычитыние строки из строки
def subtract_line(a, r):
	row = []
	for i in range(len(a)):
		row.append(a[i] - r[i])
	return row

#Умножение строки на число
def multiplication_line_num(var, r):
	row = []
	for i in range(len(r)):
		row.append(r[i] * var)
	return row

#Решение СЛАУ методом Гауса
def gauss_jordan(a, b):
	c = extended_matrix(copy_matrix(a), b)
	mass = []
	for i in range(len(c)):
		try:
			c[i] = multiplication_line_num(float(1 / c[i][i]), c[i])
		except ZeroDivisionError:
			break
		for j in range(len(c)):
			if j != i:
				row = multiplication_line_num(c[j][i], c[i])
				c[j] = subtract_line(c[j], row)
	for line in c:
		mass.append(line[-1])
	return mass

#Решение СЛАУ методом Крамера
def сramer(a, b):
	mass = []
	c = copy_matrix(a)
	det_A = determinant_matrix(a)
	for i in range(len(b)):
		for j in range(len(b)):
			c[j][i] = b[j]
			try:
				mass.append(determinant_matrix(c) / det_A)
			except ZeroDivisionError:
				break
		for j in range(len(b)):
			c[j][i] = a[j][i]
	return mass

mas_value, mas_time_1, mas_time_2, size = [], [], [], []

for i in range(2, 10, 1):
	matrix_1 = random_matrix_int(i, -50, 50)
	mas_2 = random_mas_int(i, -50, 50)

	size_mas_1 = sys.getsizeof(matrix_1)
	size_mas_2 = sys.getsizeof(mas_2)
	size.append(size_mas_1 + size_mas_2)

	avg_val_1, avg_val_2 = 0, 0

	for k in range(3):
		start_time = timer()
		n1 = gauss_jordan(matrix_1, mas_2)
		avg_val_1 += timer() - start_time

		start_time = timer()
		n2 = сramer(matrix_1, mas_2)
		avg_val_2 += timer() - start_time

	mas_value.append(i)
	mas_time_1.append(avg_val_1 / 3)
	mas_time_2.append(avg_val_2 / 3)

fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
ax1.plot(mas_value, mas_time_1, marker='o', label = 'Gauss-Jordan')
ax1.plot(mas_value, mas_time_2, marker='o', label = 'Cramer')
ax2.plot(mas_value, size, marker='o')
plt.title('График зависимости времени решения системы и объема занимаемой памяти от размерности системы')
ax1.legend(fontsize = 8, ncol = 2, facecolor = 'white', edgecolor = 'black', title = 'Прямые', title_fontsize = '8')
ax1.set_xlabel('Кол-во лин. ур-й')
ax1.set_ylabel('Время (с)')
ax2.set_xlabel('Кол-во лин. ур-й')
ax2.set_ylabel('Память (байт)')
ax1.grid()
ax2.grid()
plt.show()
